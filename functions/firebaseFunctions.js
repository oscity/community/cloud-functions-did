const admin = require("firebase-admin")

let cors_options = { origin: true }
const cors = require("cors")(cors_options)

const getCertificatesByAddress = (req, res) => {
  cors(req, res, async () => {
    const publicAddress = req.body.publicAddress
    if (!publicAddress) {
      res.status(400).send("Missing data")
    }
    try {
      const certificates = []
      await admin.firestore()
        .collection("certificates")
        .where("public_address", "==", publicAddress)
        .get()
        .then((querySnapshot) => {
          querySnapshot.forEach(doc => {
            certificates.push(doc.data())
          })
          res.status(200).send({ certificates: certificates })
        })
        .catch((error) => {
          res.status(404).send(error)
        })
    } catch (error) {
      res.status(404).send(error)
    }
  })
};

const getCertificatesByLink = (req, res) => {
  cors(req, res, async () => {
    console.log(req.body)
    const link = req.body.link
    if (!link) {
      res.status(400).send("Missing data")
    }
    try {
      const certificates = []
      await admin.firestore()
        .collection("certificates")
        .where("link", "==", link)
        .get()
        .then((querySnapshot) => {
          querySnapshot.forEach(doc => {
            certificates.push(doc.data())
          })
          res.status(200).send({ certificates: certificates })
        })
        .catch((error) => {
          res.status(404).send(error)
        })
    } catch (error) {
      console.log(error)
      res.status(404).send(error)
    }
  })
};

exports.getCertificatesByAddress = getCertificatesByAddress
exports.getCertificatesByLink = getCertificatesByLink
