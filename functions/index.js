require("dotenv").config()
const utils = require("./utils")
const jwtKey = process.env.JWT_KEY
const express = require("express")
const jwt = require("jsonwebtoken")
const admin = require("firebase-admin")
const sgMail = require("@sendgrid/mail")
const cors = require("cors")({ origin: true })
const functions = require("firebase-functions")
const serviceAccount = "../serviceAccountKey.json"
const firebaseFunctions = require("./firebaseFunctions")
const { recoverPersonalSignature } = require("eth-sig-util")

if (process.env.VERSION === "development") {
  admin.initializeApp({ credential: admin.credential.cert(serviceAccount) })
} else {
  admin.initializeApp()
}

exports.sendEmail = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    if (req.body) {
      req.body.from = {}
      req.body.from.email = process.env.EMAIL_SENDER
      req.body.from.name = process.env.EMAIL_NAME
      if (!req.body.template_id) {
        req.body.template_id = process.env.EMAIL_TEMPLATE_ID
      }
      try {
        sgMail.setApiKey(process.env.SENDGRID_API_KEY)
        await sgMail.send(req.body)
        res.sendStatus(200)
      } catch (error) {
        res.status(500).send(error)
      }
    } else {
      res.status(400).send("Missing data")
    }
  })
})

exports.createToken = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    if (req.body.currentUrl) {
      const param = req.body.uid ? req.body.uid : req.body.email
      let linkToChangePass = `${req.body.currentUrl}/`
      if (req.body.uid) {
        const token = jwt.sign({ param }, jwtKey, { expiresIn: "48h" })
        linkToChangePass += `email-action-handler?mode=${req.body.mode}&oobCode=${token}`
      } else if (req.body.email) {
        const token = jwt.sign({ param }, jwtKey)
        linkToChangePass += `register?email=${token}`
      }
      res.status(200).send(linkToChangePass)
    } else {
      res.status(400).send("Missing data")
    }
  })
})

exports.createTokenUnicef = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    const email = req.body.email
    let linkToAuthorize = ""
    if (req.body.currentUrl) {
      if (email) {
        const token = jwt.sign({ email }, jwtKey)
        linkToAuthorize = `${req.body.currentUrl}/admin/certificates/data-certificates/authorized-certificate?token=${token}`
      }
      res.status(200).send(linkToAuthorize)
    } else {
      res.status(400).send("Missing data")
    }
  })
})

exports.createMultipleTokensUnicef = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    const selected = req.body.selected
    const emails = selected.map(certificate => certificate.identity);
    const userTokens = []
    if (req.body.currentUrl) {
      if (emails) {
        emails.forEach((email) => {
          const token = jwt.sign({ email }, jwtKey)
          const linkToAuthorize = `${req.body.currentUrl}/admin/certificates/data-certificates/authorized-certificate?token=${token}`
          userTokens.push({email, linkToAuthorize})
        })
      }
      res.status(200).send(userTokens)
    } else {
      res.status(400).send("Missing data")
    }
  })
})

exports.validateToken = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    if (req.body.paramEncode) {
      jwt.verify(
        req.body.paramEncode,
        jwtKey,
        { algorithms: ["HS256"] },
        function(error, payload) {
          if (error) {
            res.status(500).send(error)
          } else {
            res.status(200).send(payload)
          }
        }
      )
    } else {
      res.status(400).send("Missing data")
    }
  })
})

exports.userExist = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    if (!req.body.email) {
      res.status(400).send("Missing data")
    }
    try {
      const user = await admin.auth().getUserByEmail(req.body.email)
      res.status(200).send({ uid: user.toJSON().uid })
    } catch (error) {
      res.status(500).send(error);
    }
  })
});

exports.userEmailExist = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    if (!req.body.uid) {
      res.status(400).send("Missing data")
    }
    try {
      const user = await admin.auth().getUser(req.body.uid)
      res.status(200).send({ user: user.toJSON() })
    } catch (error) {
      if (error.code === "auth/user-not-found") {
        res.status(404).send(error)
      } else {
        res.status(500).send(error);
      }
    }
  })
});

exports.createUser = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    if (req.body) {
      try {
        const userRecord = await admin.auth().createUser({
          email: req.body.email,
          password: req.body.password,
        })
        res.status(200).send(userRecord)
      } catch (error) {
        res.status(500).send(error)
      }
    } else {
      res.status(400).send("Missing data")
    }
  })
})

exports.userPhoneExist = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    if (!req.body.phone) {
      res.status(400).send("Missing data")
    }
    try {
      const user = await admin.auth().getUserByPhoneNumber(req.body.phone)
      res.status(200).send({ user: user.toJSON() })
    } catch (error) {
      if (error.errorInfo.code === "auth/user-not-found") {
        res.status(200).send({ user: {} })
      } else {
        res.status(500).send(error);
      }
    }
  })
});

exports.updateUser = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    if (req.body.uid) {
      try {
        const userRecord = await admin.auth().updateUser(req.body.uid, req.body.data)
        res.status(200).send(userRecord)
      } catch (error) {
        res.status(500).send(error)
      }
    } else {
      res.status(400).send("Missing data")
    }
  })
})

exports.deactivateUser = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    if (!req.body.idToken) {
      res.status(400).send("Missing data")
    } else {
      try {
        const decodedToken = await admin.auth().verifyIdToken(req.body.idToken)
        await admin.auth().updateUser(decodedToken.uid, { disabled: true })
        res.sendStatus(200)
      } catch (error) {
        res.status(500).send(error)
      }
    }
  })
})

exports.activateUser = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    try {
      if (req.body.email) {
        const userRecord = await admin.auth().getUserByEmail(req.body.email)
        await admin.auth().updateUser(userRecord.uid, { disabled: false })
        res.sendStatus(200)
      } else {
        res.status(400).send("Missing data")
      }
    } catch (error) {
      res.status(500).send(error)
    }
  })
})

exports.deleteUser = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    if (req.body.uid) {
      await admin.auth().deleteUser(req.body.uid)
      res.sendStatus(200)
    } else {
      res.status(400).send("Missing data")
    }
  })
})

exports.generateNonce = functions.https.onRequest((req, res) => {
  cors(req,res, async () => {
    const user = req.body.user
    const nonce = utils.createNonce(15)
    const data = {
      ...user,
      nonce
    }
    if(user.uid) {
      try {
        await admin.firestore().collection("users").doc(user.uid).set(data, {merge: true})
          .then(userRecord => {
            res.status(200).send(userRecord)
          })
      } catch (error) {
        res.status(500).send(error)
      }
    } else {
      res.status(400).send("Missing data")
    }
  })
})

exports.verificationSignature = functions.https.onRequest((req, res) => {
  cors(req,res, async () => {
    const { signature, publicAddress, message } = req.body
    let user
    await admin.firestore()
      .collection("users")
      .where("public_address", "==", publicAddress)
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach(doc => {
          user = doc.data()
        })
      })
    const { nonce } = user
    const address = recoverPersonalSignature({
      data: `${message}: ${nonce}`,
      sig: signature
    })
    if (address.toLowerCase() === publicAddress.toLowerCase()) {
      res.json({ ok: true, user })
    } else {
      const error = { message: "Signature verification failed" }
      res.status(500).send({ error })
    }
  })
})

exports.createCustomToken = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    try {
      if (req.body.uid) {
        const customToken = await admin.auth().createCustomToken(req.body.uid)
        res.status(200).send({ customToken })
      } else {
        res.status(400).send("Missing data")
      }
    } catch (error) {
      res.status(500).send(error)
    }
  })
})

exports.saveCollectionData = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    if(req.body.collection && req.body.data) {
      try {
        let response
        if (req.body.uid) {
          response = await admin.firestore().collection(req.body.collection).doc(req.body.uid).set(req.body.data, {merge: true})
        } else {
          response = await admin.firestore().collection(req.body.collection).add(req.body.data)
        }
        res.status(200).send(response)
      } catch (error) {
        res.status(500).send(error)
      }
    } else {
      res.status(400).send("Missing data")
    }
  })
})

/* Rentax Section */

const app = express()

app.use(cors)

exports.rentax = functions.https.onRequest(app);

/* API Section */
const api = express()
api.use(cors)

api.post("/get-certificates-by-address", firebaseFunctions.getCertificatesByAddress)
api.post("/get-certificates-by-link", firebaseFunctions.getCertificatesByLink)

exports.api = functions.https.onRequest(api);
